package cn.ttg.controller;

import cn.ttg.dto.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class FormController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FormController.class);


    @RequestMapping(value = "/form",method = RequestMethod.POST)
    public ResponseEntity<Department> form(Department department){
        LOGGER.info("department:{}",department);
        return new ResponseEntity<>(department, HttpStatus.OK);
    }
}
