package cn.ttg.dto;

import lombok.Data;

@Data
public class User {
    private String username;
    private String password;
}
