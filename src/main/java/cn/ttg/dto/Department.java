package cn.ttg.dto;

import lombok.Data;

import java.util.List;

@Data
public class Department {
    private String id;
    private List<User> users;
}
