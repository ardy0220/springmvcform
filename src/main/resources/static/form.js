$(function () {

    $("#go").click(function () {
        console.log($("#action").serializeArray());
        $.ajax({
            url: "/form",
            data: $("#action").serializeArray(),
            type: "POST",
            dataType: "json",
            success: function (result) {
                console.log(result);
                $("#result").text(JSON.stringify(result));
            }
        });
    })


});